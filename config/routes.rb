Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  root to: "pages#home"

  # get '/posts', to: 'posts#index'
  # get '/posts/new', to: 'posts#new'
  # post '/posts', to: 'posts#create'
  # get '/posts/:id', to: 'posts#show', as: 'model'
  # get '/posts/:id/edit', to: 'posts#edit'
  # put '/posts/:id', to: 'posts#update'
  # delete '/posts/:id', to: 'posts#destroy'

  resources :posts
  # Defines the root path route ("/")
  # root "articles#inde
end
